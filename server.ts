import * as Express from "express";
import * as BodyParser from "body-parser";

export function init() {
  const app = Express();

  app.use(BodyParser.urlencoded({ extended: true}));
  app.use(BodyParser.json());
  app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin',
                  '*');
    res.setHeader('Access-Control-Allow-Methods',
                  'GET, POST, HEAD');
    res.setHeader('Access-Control-Allow-Headers',
                  'X-Requested-With, content-type, Authorization');
    next();
  });
  
  return app;
};
