export const ERRORS = {
    internalServerError: 'Internal Server Error',
    badRequest: 'Bad Request',
    conflict: 'Conflict'
  };
  
  export const HTTP_CODES = {
    ok: 200,
    created: 201,
    noContent: 204,
    badRequest: 400,
    unauthorized: 401,
    conflict: 409,
    internalServerError: 500,
  
  };
  
  export const MONGO_ERRORS = {
    uniqueError: 11000,
    validationErrorName: 'ValidationError'
  }