import * as Mongoose from "mongoose";

export interface IExample extends Mongoose.Document {
  name: string;
  description: string;
  creationDate: Date;
}
export const ExampleSchema = {
  name: { type: String, required: [true, 'mensagem'] },
  description: String,
  creationDate: { type: Date, default: Date.now},
};
