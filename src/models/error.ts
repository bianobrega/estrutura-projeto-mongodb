export interface IError {
  type: string;
  errors: IErrorMessage[];
};

export interface IErrorMessage {
	message: string;
};