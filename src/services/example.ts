import { IDatabase } from "../../database";
import { DatabaseService }  from './database';
import { IExample } from '../models/example'

export default class ExampleService extends DatabaseService <IExample> {
  constructor (dataBase: IDatabase) {
    super(dataBase.exampleModel);
  }
}