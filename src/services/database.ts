import * as Mongoose from "mongoose";
import * as ErrorUtil from "../commons/util/error";

export abstract class DatabaseService<T> {
  private model: Mongoose.model;
  
  constructor (model: Mongoose.model) {
    this.model = model;
  }

  public async findAll() : Promise<T[]> {
    try {
      return await this.model.find({});
    } catch (error) {
      console.log('[DataBaseService - findAll]: ', error);
      throw ErrorUtil.generateErrorService(error); 
    }
  };

  public async add(document: Mongoose.document) : Promise<T> {
    try {
      return await this.model.create(document);
    } catch (error) {
      console.log('[DataBaseService - add]: ', error);
      throw ErrorUtil.generateErrorService(error);
    }    
  }

  public async findById(id: string) : Promise<T> {
    try {
      return await this.model.findOne({_id: id});
    } catch (error) {
      console.log('[DataBaseService - findById]: ', error);
      throw ErrorUtil.generateErrorService(error);
    }
  }
  
  public update (id: string, document: Mongoose.document) : Promise<T> {
    try {
      return this.model.findByIdAndUpdate({_id: id}, {$set: document}, {new: true});
    } catch (error) {
      console.log('[DataBaseService - update]: ', error);
      throw ErrorUtil.generateErrorService(error); 
    }
  }

  public remove (id: string) {
    try {
      return this.model.remove({ _id: id});
    } catch (error) {
      console.log('[DataBaseService - remove]: ', error);
      throw ErrorUtil.generateErrorService(error);
    }
  }
}