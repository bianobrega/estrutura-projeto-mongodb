import * as Express from "express";

import ExampleController from "../controllers/example";
import { IDatabase } from "../../database";
import { IServerConfigurations } from "../../config";

export default function (server, serverConfigs: IServerConfigurations, database: IDatabase) {
  const exampleController = new ExampleController(serverConfigs, database);

  server.post('/v1/example', exampleController.add.bind(exampleController));
  server.put('/v1/example/:id', exampleController.update.bind(exampleController));
  server.get('/v1/example/list', exampleController.findAll.bind(exampleController));
  server.get('/v1/example/:id', exampleController.findById.bind(exampleController));
}
