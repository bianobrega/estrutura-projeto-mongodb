import * as Mongoose from "mongoose";
import { IDataConfiguration } from "./config";
import { IExample, ExampleSchema } from "./src/models/example";

export interface IDatabase {
  exampleModel: Mongoose.Model<IExample>;
}

export function init(config: IDataConfiguration): IDatabase {
  console.log('CONFIG');
  console.log(config);

  (<any>Mongoose).Promise = Promise;

  const connection = Mongoose.createConnection(config.connectionString);

  connection.on('error', () => {
    console.log('Unable to connect to database: ' + config.connectionString);
  });

  connection.once('open', () => {
    console.log('Connected to database: ' + config.connectionString);
  });

  const ExampleModel = connection.model<IExample> ('Example', ExampleSchema);

  return {
    exampleModel: ExampleModel,
  };
}