import * as Server from "./server";
import * as Configs from "./config";
import * as Example from "./src";
import * as Database from "./database";

console.log(`Running enviroment ${process.env.NODE_ENV || "dev"}`);

const serverConfigs = Configs.getServerConfigs();

//Init Database
const dbConfigs = Configs.getDatabaseConfig();
const database = Database.init(dbConfigs);

//Starting Application Server
const server = Server.init();

server.listen(process.env.port || serverConfigs.port, function() {
  console.log('Servidor ON');
  
  Example.init(server, serverConfigs, database);
});